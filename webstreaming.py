import base64
import threading
import time
from argparse import ArgumentParser
from datetime import datetime
from typing import Optional
import inject

import cv2
import fastapi
import imutils
import uvicorn
import websockets
from fastapi.middleware.cors import CORSMiddleware
from imutils.video import VideoStream
from functools import partial

from object_detection import ObjectDetector, SingleMotionDetector, YOLOObjectDetector


class WebSteaming:
    def __init__(self, resize_width: int = 400, frame_count: int = 32):
        self._frame_count = frame_count
        self._resize_width = resize_width

        self._outputFrame = None
        self._lock = threading.Lock()
        self._video_stream: Optional[VideoStream] = None

        self._video_stream_thread: Optional[threading.Thread] = None
        self._thread_working = False

    def __enter__(self):
        self._video_stream = VideoStream(src=0).start()

        self._thread_working = True
        self._video_stream_thread = threading.Thread(target=self._detect_motion, args=(self._frame_count,))
        self._video_stream_thread.daemon = True
        self._video_stream_thread.start()
        time.sleep(0.2)

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._video_stream.stop()
        self._video_stream = None

        self._thread_working = False
        self._video_stream_thread.join()

    @inject.params(object_detector=ObjectDetector)
    def _detect_motion(self, frame_count: int, object_detector: ObjectDetector):
        total = 0

        while self._thread_working:
            frame = self._video_stream.read()
            # frame = imutils.resize(frame, width=self._resize_width)
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            gray = cv2.GaussianBlur(gray, (7, 7), 0)

            timestamp = datetime.now()
            cv2.putText(frame, timestamp.strftime("%A %d %B %Y %I:%M:%S%p"), (10, frame.shape[0] - 10),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)

            if total > frame_count:
                motion = object_detector.detect(frame)

                if motion is not None:
                    contours = motion
                    for x, y, w, h in contours:
                        cv2.rectangle(frame, (x, y), (w, h), (0, 0, 255), 2)

            object_detector.update(gray)
            total += 1

            with self._lock:
                flag, encoded_image = cv2.imencode('.jpg', frame)
                self._outputFrame = encoded_image

    def _generate_image(self):
        while self._thread_working:
            with self._lock:
                if self._outputFrame is None:
                    continue

                output_frame = self._outputFrame.copy()

            yield bytearray(output_frame)

    @staticmethod
    def index():
        with open("templates/index.html") as f:
            return f.read()

    def create_app(self):
        app = fastapi.FastAPI()

        app.add_route('/', self.index, methods=["GET"])
        app.add_websocket_route('/video_feed', self.video_feed)

        return app

    async def video_feed(self, websocket: fastapi.websockets.WebSocket):
        try:
            await websocket.accept()
            for bytes in self._generate_image():
                await websocket.send_bytes(base64.b64encode(bytes))
                await websocket.receive_json()
        except (fastapi.websockets.WebSocketDisconnect, websockets.exceptions.ConnectionClosedError):
            pass


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument('--ip', type=str, required=True, help='ip address if the device')
    parser.add_argument('--port', type=int, default=8000, help="port to listen to")
    parser.add_argument('--motion_image_width', type=int, default=720, help='width of the motion image')
    parser.add_argument('--frame_count', type=int, default=100,
                        help="# if frames used to construct the background model")
    parser.add_argument('--t_val', type=int, default=25, help='threshold value')
    parser.add_argument('--accum_weight', type=float, default=0.0001, help='accumulative weight')

    args = parser.parse_args()
    return args.ip, args.port, args.motion_image_width, args.frame_count, args.t_val, args.accum_weight


def configure_binder(binder: inject.Binder):
    binder.bind(ObjectDetector, YOLOObjectDetector('yolo-coco/yolov3-tiny.weights', 'yolo-coco/yolov3-tiny.cfg', 0.1))


def main():
    ip, port, motion_image_width, frame_count, t_val, accum_weight = parse_arguments()
    inject.configure(partial(configure_binder, accum_weight=accum_weight, t_val=t_val))

    with WebSteaming(motion_image_width, frame_count) as web_steaming:
        app = web_steaming.create_app()
        origins = [
            "*",
        ]

        app.add_middleware(
            CORSMiddleware,
            allow_origins=origins,
            allow_credentials=True,
            allow_methods=["*"],
            allow_headers=["*"],
        )
        uvicorn.run(app, host=ip, port=port, debug=True)


if __name__ == '__main__':
    main()
