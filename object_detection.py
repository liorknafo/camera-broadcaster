from abc import ABC, abstractmethod
from typing import Tuple, List

import imutils
import numpy as np
import cv2


class ObjectDetector(ABC):
    @abstractmethod
    def update(self, image: np.ndarray):
        pass

    @abstractmethod
    def detect(self, image: np.ndarray) -> List[Tuple[int, int, int, int]]:
        pass


class SingleMotionDetector(ObjectDetector):
    def __init__(self, accum_weight: float = 0.5, t_val: int = 25):
        self._accumWeight = accum_weight
        self._t_val = t_val
        self._background = None

    def update(self, image: np.ndarray):
        if self._background is None:
            self._background = image.copy().astype('float32')

        cv2.accumulateWeighted(image, self._background, self._accumWeight)

    def detect(self, image: np.ndarray) -> List[Tuple[int, int, int, int]]:
        delta = cv2.absdiff(self._background.astype('uint8'), image)
        thresh = cv2.threshold(delta, self._t_val, 255, cv2.THRESH_BINARY)[1]

        thresh = cv2.erode(thresh, None, iterations=2)
        thresh = cv2.dilate(thresh, None, iterations=2)

        contours = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        contours = imutils.grab_contours(contours)
        min_x, min_y = np.inf, np.inf
        max_x, max_y = -np.inf, -np.inf

        if len(contours) == 0:
            return None

        for contour in contours:
            x, y, w, h = cv2.boundingRect(contour)
            min_x, min_y = min(min_x, x), min(min_y, y)
            max_x, max_y = max(max_x, x), max(max_y, y)

        return thresh, [(min_x, min_y, max_x, max_y)]


class MultiMotionDetector(ObjectDetector):
    def __init__(self, accum_weight: float = 0.5, t_val: int = 25):
        self._accumWeight = accum_weight
        self._t_val = t_val
        self._background = None

    def update(self, image: np.ndarray):
        if self._background is None:
            self._background = image.copy().astype('float32')

        cv2.accumulateWeighted(image, self._background, self._accumWeight)

    def detect(self, image: np.ndarray) -> List[Tuple[int, int, int, int]]:
        delta = cv2.absdiff(self._background.astype('uint8'), image)
        thresh = cv2.threshold(delta, self._t_val, 255, cv2.THRESH_BINARY)[1]

        thresh = cv2.erode(thresh, None, iterations=2)
        thresh = cv2.dilate(thresh, None, iterations=6)

        contours = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        contours = imutils.grab_contours(contours)

        if len(contours) == 0:
            return None

        contours = [cv2.boundingRect(contour) for contour in contours]
        contours = [(x, y, x + w, y + h) for x, y, w, h in contours]

        return thresh, contours


class YOLOObjectDetector(ObjectDetector):
    def __init__(self, weights_path: str, config_path: str, threshold: float = 0.5, ):
        self._threshold = threshold
        self._net = cv2.dnn.readNetFromDarknet(config_path, weights_path)

        self._ln = self._net.getLayerNames()
        self._ln = [self._ln[i[0] - 1] for i in self._net.getUnconnectedOutLayers()]

    def detect(self, image: np.ndarray) -> List[Tuple[int, int, int, int]]:
        blob = cv2.dnn.blobFromImage(image, 1 / 255.0, (416, 416), swapRB=True, crop=False)
        self._net.setInput(blob)
        layer_output = self._net.forward(self._ln)
        (H, W) = image.shape[:2]

        boxes = []

        for output in layer_output:
            for detection in output:
                scores = detection[5:]
                classID = np.argmax(scores)
                confidence = scores[classID]
                if confidence > self._threshold and classID == 14:
                    box = detection[0:4] * np.array([W, H, W, H])
                    (centerX, centerY, width, height) = box.astype("int")

                    x = int(centerX - (width / 2))
                    y = int(centerY - (height / 2))

                    boxes.append((x, y, x + int(width), y + int(height)))
                    # confidences.append(float(confidence))
                    # classIDs.append(classID)

        return boxes

    def update(self, image: np.ndarray):
        pass